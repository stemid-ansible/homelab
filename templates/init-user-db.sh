#!/usr/bin/env bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<EOSQL
  create role grafana with login;
  create database grafana;
  alter database grafana owner to grafana;
  alter user grafana with password '{{ grafana_db_password }}';
EOSQL

#psql -U grafana grafana </docker-entrypoint-initdb.d/grafana.sql
