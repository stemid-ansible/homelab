---

- hosts: container_host

  vars_files:
    - vars/all.yml
    - "secrets/{{ site_inventory }}.yml"

  handlers:
    - name: restart container services
      systemd:
        name: "container-{{ item.name }}"
        state: restarted
        scope: user
      when: item.generate_systemd is defined
      with_items: "{{ containers }}"

  roles:
    - role: ansible-systemd-timers
      systemd_scope: user

  tasks:
    - name: remove containers
      containers.podman.podman_container:
        name: "{{ item.name }}"
        state: absent
      with_items: "{{ containers }}"
      when: remove_containers | default(False) | bool
      tags: containers

    - name: install redis for redis-cli tool
      package:
        name: redis
        state: present
      become: True
      tags: config

    - name: open ports
      firewalld:
        port: "{{ item.port }}"
        permanent: "{{ item.permanent | default('yes') }}"
        state: "{{ item.state | default('enabled') }}"
      become: True
      with_items: "{{ container_ports }}"
      tags: config

    - debug:
        var: container_files_path

    - name: create container files dir
      file:
        path: "{{ container_files_path }}"
        state: directory
        mode: 0750
      tags: config

    - name: install container files
      template:
        src: "{{ item.src }}"
        dest: "{{ container_files_path }}/{{ item.dest | default(item.src) }}"
        mode: "{{ item.mode | default('0740') }}"
        setype: container_file_t
        owner: "{{ item.user | default(admin_user) }}"
        group: "{{ item.group | default(omit) }}"
      with_items: "{{ container_files }}"
      notify: restart container services
      tags: config

    - name: create container networks
      containers.podman.podman_network:
        name: "{{ item.name }}"
        internal: "{{ item.internal | default(omit) }}"
      with_items: "{{ container_networks }}"
      tags: containers

    - name: install containers
      containers.podman.podman_container:
        name: "{{ item.name }}"
        state: "{{ item.state }}"
        image: "{{ item.image }}"
        restart: "{{ item.restart | default('no') }}"
        restart_policy: "{{ item.restart_policy | default('no') }}"
        ports: "{{ item.ports | default(omit) }}"
        generate_systemd: "{{ item.generate_systemd | default(omit) }}"
        env: "{{ item.env | default(omit) }}"
        env_file: "{{ item.env_file | default(omit) }}"
        expose: "{{ item.expose | default(omit) }}"
        device: "{{ item.device | default(omit) }}"
        volume: "{{ item.volume | default(omit) }}"
        network: "{{ item.network | default(omit) }}"
        cmd_args: "{{ item.cmd_args | default(omit) }}"
      with_items: "{{ containers }}"
      ignore_errors: True
      tags: containers

    - name: systemctl daemon-reload
      systemd:
        daemon_reload: yes
        scope: user
      environment:
        XDG_RUNTIME_DIR: "/run/user/{{ ansible_user_uid }}"
      tags: systemd

    - name: enable all container systemd services
      systemd:
        name: "container-{{ item.name }}"
        state: started
        enabled: yes
        scope: user
      with_items: "{{ containers }}"
      when: item.generate_systemd is defined and item.state == 'present'
      tags: systemd

