# My homelab

![Homelab network architecture diagram](/files/diagram/Homelab.png "Homelab network architecture")

* This is very personal to my homelab but maybe someone can take some value from it.
* Inventory setup in ``inventory/my-env/hosts``, along with any ``host_vars`` or ``group_vars`` in the same directory.
* All ansible commands in this README omit the ``-i`` and ``--vault-id`` arguments for brevity.
* See the example inventory environment for more info, I try to keep it up2date.

## Secrets

Put secrets in ``secrets/{{ site_inventory }}.yml``, ``site_inventory`` being defined in your hosts inventory like this;

```
[all:vars]
site_inventory=my-env
```

# Gateway

* Playbook: gateway.yml
* Tags: bootstrap, unbound, dns, pf, firewall, dhcpd, dhcp, pxe

## PF rules testing

The role firewall used in gateway.yml only tests and displays the pf rules, it won't run them yet. Use ``ANSIBLE_STDOUT_CALLBACK=debug`` to see the pfctl output correctly.

    ANSIBLE_STDOUT_CALLBACK=debug ansible-playbook gateway.yml -t firewall

### TODO: Maybe apply the rules with a boolean variable

    ansible-playbook -e 'apply_firewall_rules=True' -t firewall gateway.yml

## PXE server

Requires some files in files/pxelinux, here is how to get them.

Mount the RHEL 8 DVD ISO and extract from BaseOS/Packages/syslinux-tftpboot*.rpm tftpboot/pxelinux.0.

Then from images/pxeboot/ copy vmlinuz and initrd.img.

[See also RedHat docs](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/installation_guide/chap-installation-server-setup#sect-network-boot-setup-uefi).

### Re-Provisioning a node

Just ensure the hosts have their EFI/BIOS set to use PXE as a fallback boot option, then run this command on the host you want to recycle.

    sudo shutdown -r +1 && sleep 30 && \
        sudo dd if=/dev/zero of=/dev/sda bs=1M count=10 conv=fsync status=progress

This will schedule a reboot in 1 minute and overwrite the first 10MB of the disk. Replace ``/dev/sda`` with whatever system disk you have.

## Dynamic DNS

* Playbook: dyndns.yml
* Tags: dns

Install crontab and script that maintains dynamic DNS in AWS Route53.

All of these environment variables are read from current environment when ansible-playbook runs, and installed into remote server's crontab environment, for the script.

* ``AWS_ACCESS_KEY_ID`` - Self-explanatory
* ``AWS_SECRET_ACCESS_KEY`` - Self-explanatory
* ``ROUTE53_DOMAIN`` - Name of subdomain to update, in a Routge53 Hosted zone.
* ``CACHE_FILENAME`` - Filename to store a cache of last IP, to compare current IP to.

# Wireguard

* Playbook: wireguard.yml
* Tags: wireguard, vpn, server, clients

## How to setup wireguard key

Create the wireguard server keys locally first.

    mkdir -p files
    wg genkey > files/server.secret.key
    wg pubkey < files/server.secret.key > files/server.public.key

Encrypt the secret key into Ansible vault format.

    cat files/server.secret.key | ansible-vault encrypt_string --vault-id=my-env@.vault-password.my-env

Copy and paste the secret key in encrypted form to a variable in ``secrets/my-env.yml``.

```yaml
wg_secret_key: !vault |
    $ANSIBLE_VAULT;1.2;AES256;my-env
    XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    XXXXXXXXXXXXXXXXXXXXXXXX...
```

## Add wireguard client

### Create a keypair for the client manually

To create a client config named smartphone, from the config template templates/client.conf.

    bash tools/gen_client.sh smartphone

### Add the client and their pubkey to wg_clients list

Put this either in your inventory vars or secrets/ dir.

```yaml
wg_clients:
  - name: smartphone
    pubkey: 'qwertyxxxx='
    allowedIPs: 10.0.0.10/32
```

# K8s

* Playbook: k8s.yml
* Tags: bootstrap, runtime, k8s, control, worker, post, cleanup
* Supports only one control plane server for now.
* IPv4 disabled by default, both in host OS and CNI.

## Running

    ansible-playbook k8s.yml -t bootstrap,runtime
    ansible-playbook k8s.yml -t k8s,control,worker

>**Note:** The bootstrap tag tries to register a Red Hat subscription if the ``rhel_subscription`` dict is set.

## Add new worker node

    ansible-playbook k8s-yml -t control,worker

>**Note**: The join command is created in the control tag (role:k8s-control) and then used in the worker tag (role:k8s-worker), so you must run both of these tags for joining new nodes.

## Patching

    ansible -m command -a 'dnf update -y' -b k8s
    ansible -m command -a 'shutdown -r +1' -b k8s

## cri-o cheatsheet

    sudo crictl ps -a

# Known issues

## Adding new hosts in local_devices list

* Unbound must be restarted before dhcpd.conf can be installed because dhcpd will try to resolve all fixed-address names. Run with ``-t dns`` first and then ``-t dhcp,firewall``.
