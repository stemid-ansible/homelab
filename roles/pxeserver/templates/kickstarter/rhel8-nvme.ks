# RHEL 8
cmdline

# My nodes all have one /dev/sda and one /dev/nvme, so this works for me.
ignoredisk --only-use=/dev/nvme0n1

zerombr
clearpart --all --initlabel --disklabel gpt

# nopti noibrs noibpb disable CVE-2017-5754, CVE-2017-5753, and CVE-2017-5715
bootloader --timeout 1 --append "nopti noibrs noibpb console=tty0 console=ttyS0,115200 ipv6.disable=1 quiet systemd.show_status=yes"
part biosboot  --fstype biosboot --size 1
part /boot/efi --fstype efi      --size 63
part /boot     --fstype xfs      --size 1024
#part swap      --fstype swap     --size 1024
part /         --fstype xfs      --size 10240 --grow

skipx

{% for user in kickstart_admin_users %}
user --name={{ user.name }} --groups=wheel --shell=/bin/bash --lock
{% endfor %}

selinux --permissive
authselect --useshadow --passalgo sha512
rootpw --iscrypted {{ kickstart_rhel8_root_password }}

# Debug
#rootpw root

# Network does not work, use ifcfg file in %post instead
#network --device=1c:69:7a:63:f8:7c --bootproto=dhcp --onboot=yes --noipv6
network --device=link --bootproto=dhcp --onboot=yes --noipv6
firewall --disabled
firstboot --disabled
lang en_US.UTF-8
timezone --ntpservers {{ kickstart_ntpservers | default('127.0.0.1') }} --utc {{ ks_timezone | default('Europe/Copenhagen') }}
keyboard --vckeymap=se
services --enabled tuned
reboot

# Options must be kept in sync with the below Packages - trimming section
%packages --instLangs {{ ks_pkg_install_languages | default('en_US') }}
# --excludedocs
# --excludeWeakdeps
# --ignoremissing
# --nocore
@Core
bash-completion
bind-utils
#cloud-init
#cloud-utils-growpart
glibc-minimal-langpack
man-pages
NetworkManager-config-server
policycoreutils-python-utils
prefixdevname
psmisc
python3
python3-libselinux
setools-console
tar
#unzip
vim-enhanced
yum-utils

%end

%post --log=/root/ks-post.log
#nmcli con add connection.type 802-3-ethernet connection.id net0 connection.interface-name net0 connection.autoconnect yes ipv4.method auto

#cat <<EOF >/etc/sysconfig/network-scripts/ifcfg-net0
#TYPE=Ethernet
#BOOTPROTO=dhcp
#DEFROUTE=yes
#IPV6INIT=no
#NAME=net0
#DEVICE=net0
#ONBOOT=yes
#EOF

{% for user in kickstart_admin_users %}
mkdir {{ user.home | default('/home/'+user.name) }}/.ssh
echo '{{ user.ssh_key }}' >{{ user.home | default('/home/'+user.name) }}/.ssh/authorized_keys
chown -R {{ user.name }}:{{ user.group | default(user.name) }} {{ user.home | default('/home/'+user.name) }}/.ssh
{% endfor %}

cat <<EOF >/etc/sudoers.d/wheel
%wheel ALL=NOPASSWD: ALL
Defaults:%wheel timestamp_timeout=30
EOF
chmod 0440 /etc/sudoers.d/wheel

echo 'IPv4: \4' >> /etc/issue

# GRUB / console
#sed -i -e 's,GRUB_TERMINAL.*,GRUB_TERMINAL="serial console",' /etc/default/grub
#sed -i -e '/GRUB_SERIAL_COMMAND/d' -e '$ i GRUB_SERIAL_COMMAND="serial --speed=115200"' /etc/default/grub
#sed -i -e 's/console=tty0 //' -e 's/ console=ttyS0,115200//' /etc/default/grub
sed -i -e 's,rhgb ,,g' /etc/default/grub
test -f /boot/grub2/grub.cfg && grubcfg=/boot/grub2/grub.cfg || grubcfg=/boot/efi/EFI/redhat/grub.cfg
grub2-mkconfig -o $grubcfg
grub2-editenv - unset menu_auto_hide

# IPv6
grep -q ipv6.disable=1 /etc/default/grub && ipv6=no || ipv6=yes
if [ "$ipv6" = "no" ]; then
  sed -i -e '/^::1/d' /etc/hosts
  sed -i -e 's,^OPTIONS=",OPTIONS="-4 ,g' -e 's, ",",' /etc/sysconfig/chronyd
  sed -Ei -e 's,^(#|)AddressFamily .*,AddressFamily inet,' /etc/ssh/sshd_config
  sed -i -e 's,^IPv6_rpfilter=yes,IPv6_rpfilter=no,' /etc/firewalld/firewalld.conf
  sed -i -e '/dhcpv6-client/d' /etc/firewalld/zones/public.xml
fi

%end
