- name: create pxeserver directory
  file:
    path: "{{ pxeserver_directory }}"
    state: directory
    mode: 0755

- name: enable tftpd service
  service:
    name: tftpd
    enabled: yes

- name: set tftpd startup flags
  command: "rcctl set tftpd flags {{ pxeserver_directory }}"
  changed_when: False

- name: start tftpd service
  service:
    name: tftpd
    state: started

- name: create tftp directories for RHEL 8
  file:
    path: "{{ item }}"
    state: directory
    mode: 0755
  with_items:
    - "{{ pxeserver_directory }}/rhel-8/pxelinux/pxelinux.cfg"
    - "{{ pxeserver_directory }}/rhel-8/images/RHEL-8.6"

- name: install pxelinux.cfg/default boot config for rhel8
  template:
    src: default
    dest: "{{ pxeserver_directory }}/rhel-8/pxelinux/pxelinux.cfg/default"

- name: install grub.cfg boot config per hwaddr
  template:
    src: grub.cfg
    dest: "{{ pxeserver_directory }}/rhel-8/grub.cfg-01-{{ item.hwaddr | replace(':', '-') }}"
  when: item.pxe is defined
  with_items: "{{ local_devices }}"

- name: transfer pxelinux binaries
  copy:
    src: "{{ item.src }}"
    dest: "{{ item.dest }}"
    mode: 0644
  with_items:
    - src: files/pxeserver/grubx64.efi
      dest: "{{ pxeserver_directory }}/rhel-8/grubx64.efi"
    - src: files/pxeserver/shimx64.efi
      dest: "{{ pxeserver_directory }}/rhel-8/shimx64.efi"
    - src: files/pxeserver/pxelinux.0
      dest: "{{ pxeserver_directory }}/rhel-8/pxelinux/pxelinux.0"
    - src: files/pxeserver/initrd.img
      dest: "{{ pxeserver_directory }}/rhel-8/images/RHEL-8.6/initrd.img"
    - src: files/pxeserver/vmlinuz
      dest: "{{ pxeserver_directory }}/rhel-8/images/RHEL-8.6/vmlinuz"

- name: enable and start tftpproxy
  service:
    name: tftpproxy
    state: started
    enabled: yes

- name: install kickstart config for rhel8
  template:
    src: "kickstarter/{{ item.src }}"
    dest: "{{ nginx_webroot }}/{{ item.dest | default(item.src) }}"
    group: daemon
    mode: 0644
  with_items:
    - src: rhel8.ks
    - src: rhel8-nvme.ks

- name: ensure nginx is started
  service:
    name: nginx
    state: started
    enabled: yes
