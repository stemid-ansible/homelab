#!/usr/bin/env bash

/usr/bin/kubeadm init \
  --service-dns-domain='{{ k8s_cluster_name }}' \
  --apiserver-cert-extra-sans='{{ k8s_apiserver_cert_extra_sans }}' \
  --node-name '{{ kubernetes_node_name | default(ansible_nodename) }}' \
  --pod-network-cidr='{{ k8s_pod_network_cidr }}' \
  --control-plane-endpoint='{{ k8s_control_plane_endpoint }}' "$@"
