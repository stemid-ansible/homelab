#!/usr/bin/env python

# # Dynamic DNS update script for AWS Route53
#
# This should run directly on your gateway because the only supported method needs access to your external IP directly on a local Network interface.
#
# Depends on boto3 and click.
#
# by Stefan Midjich ``<swehack at gmail punkt com>`` 2022/11


# Getting IP from NIC
import socket
import fcntl
import struct

# Other corelibs
import json
import atexit
import logging
from os import stat
from sys import exit, stdout

# 3rd party imports
import click
import boto3
from boto3.session import Session

def write_filecache(cache, filename):
    logging.info('Writing filecache to {filename}'.format(
        filename=filename
    ))
    with open(filename, 'w') as cache_file:
        json.dump(cache, cache_file)

def getip(connect_host, connect_port):
    """Get NIC of outgoing route as a quick way to get the public IP"""
    logging.info('Performing outgoing socket connection to {}:{}'.format(
        connect_host,
        connect_port
    ))
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect((connect_host, connect_port))
    return s.getsockname()[0]


@click.command()
@click.option(
    '--access-key-id', 'key_id',
    metavar='AWS_ACCESS_KEY_ID',
    envvar='AWS_ACCESS_KEY_ID',
    required=True,
    help='AWS Access Key ID'
)
@click.option(
    '--secret-access-key', 'secret_key',
    metavar='AWS_SECRET_ACCESS_KEY',
    envvar='AWS_SECRET_ACCESS_KEY',
    required=True,
    help='AWS Secret Access Key'
)
@click.option(
    '--domain', 'domain',
    metavar='ROUTE53_DOMAIN',
    envvar='ROUTE53_DOMAIN',
    required=True,
    help='Route53 domain to update'
)
@click.option(
    '--cache-filename', 'cache_filename',
    metavar='CACHE_FILENAME',
    envvar='CACHE_FILENAME',
    type=click.Path(exists=False, readable=True, writable=True),
    default='cache.json',
    help='Cache filename for IP-address'
)
@click.option(
    '--connect-host', 'host',
    metavar='HOST',
    default='8.8.4.4',
    help='Connect to host to get public IP of outgoing NIC'
)
@click.option(
    '--connect-port', 'port',
    metavar='PORT',
    type=int,
    default=80,
    help='Connect to port to get public IP of outgoing NIC'
)
@click.option('-v', '--verbose', count=True, help='>2 = dry-run')
def update(key_id, secret_key, domain, cache_filename, host, port, verbose):
    """Each argument takes an environment variable matching the meta text,
    like for example AWS_ACCESS_KEY_ID and CACHE_FILENAME."""

    if verbose == 1:
        loglevel = logging.INFO
    elif verbose > 1:
        loglevel = logging.DEBUG
    else:
        loglevel = logging.WARNING
    logging.basicConfig(stream=stdout, level=loglevel)

    # Init file cache for quick runs when IP has not changed
    try:
        if stat(cache_filename).st_size > 3:
            with open(cache_filename, 'r') as cache_file:
                cache = json.load(cache_file)
                logging.info('Cache loaded from file')
    except FileNotFoundError:
        cache = {}
        logging.info('Empty cache created')
        pass

    # Get public IP from outgoing NIC
    try:
        ip = getip(host, port)
    except Exception as e:
        logging.error('Failed to get IP from outgoing NIC, check network connection')
        if verbose:
            print(str(e))

    if not len(ip):
        logging.error('No IP found on outgoing NIC')
        exit(1)

    # Check if ip deviates from cache
    if len(cache):
        if 'ip' in cache:
            if ip == cache['ip']:
                logging.info('IP in cache same as on NIC, exiting')
                exit(0)
        else:
            cache['ip'] = ''
    else:
        cache['ip'] = ip

    logging.info('Discovered IP {} on NIC'.format(ip))
    atexit.register(write_filecache, cache, cache_filename)

    logging.info('Establishing AWS session')
    session = Session(
        aws_access_key_id=key_id,
        aws_secret_access_key=secret_key
    )
    client = session.client('route53')

    # Get zone ID first
    logging.info('Searching for hosted zone with {}'.format(domain))
    response = client.list_hosted_zones_by_name(
        DNSName='.'.join(domain.split('.')[-2:])
    )
    for zone in response['HostedZones']:
        if domain.endswith(zone['Name'].rstrip('.')):
            logging.debug('Found matching hosted zone {}'.format(zone['Name']))
            zone_id = zone['Id']
            break
        logging.debug('Skipping hosted zone {}'.format(zone['Name']))
    else:
        logging.error('Could not find zone ID based on domain')
        exit(1)
    logging.info('Found zone id {} for domain {}'.format(zone_id, domain))

    # Update resource record
    logging.info('Updating resource record set in Route53')

    if verbose > 2:
        logging.debug('Dry-run detected, exiting before resource record update')
        exit(0)

    try:
        client.change_resource_record_sets(
            HostedZoneId=zone_id,
            ChangeBatch={
                'Comment': 'Modified by dyndns updater.py script',
                'Changes': [
                    {
                        'Action': 'UPSERT',
                        'ResourceRecordSet': {
                            'TTL': 300,
                            'Name': domain,
                            'Type': 'A',
                            'ResourceRecords': [
                                {
                                    'Value': ip
                                }
                            ]
                        }
                    }
                ]
            }
        )
    except Exception as e:
        if verbose:
            raise
        else:
            logging.error(str(e))
        exit(1)


if __name__ == '__main__':
    exit(update(None))
