# Dynamic DNS for AWS Route53

This role can be broken off for re-use in other places. The role does not require root.

It uses local environment values to create a cron job on the remote node. So these environment values should only exist on the Ansible control node when you run Ansible, then they are transferred to the remote node. I use [direnv](https://direnv.net/) for this.

* ``AWS_ACCESS_KEY_ID``
* ``AWS_SECRET_ACCESS_KEY``
* ``ROUTE53_DOMAIN`` - The full domain you want to update, like ``vpn.domain.tld`` for example.
* ``CACHE_FILENAME`` - Where to store a cache of the IP so the script does nothing unless the IP is changed.

## Role parameters

* ``cronjob_hour`` - Defaults to */2
* ``cronjob_minute`` - Defaults to 0

# Python script templates/updater.py

* Depends on boto3 and click as 3rd party packages, but the role will install them and set them up in a virtualenv without using root.
* Has a cache file so if the IP is not changed it will simply exit, meaning you could probably run this every minute if you wanted a high frequency.
* Is designed to run on your gateway because the script looks up your public IP directly on the default route interface, not using any external service like [ifconfig.me](https://ifconfig.me).
* It does this by creating an outbound socket to 8.8.4.4 (Google's public DNS resolver) and thereby using the egress interface through the default route, so no interface name needs to be specified either.

# AWS access control

Create a new IAM user and add an inline JSON policy similar to this one for access control to just one hosted zone.

```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid" : "AllowResourceRecordSetUpdate",
            "Effect": "Allow",
            "Action": [
                "route53:GetHostedZone",
                "route53:ListResourceRecordSets",
                "route53:ChangeResourceRecordSets"
            ],
            "Resource": "arn:aws:route53:::hostedzone/XXXXID"
        },
        {
            "Sid" : "AllowListingHostedZones",
            "Effect": "Allow",
            "Action": [
                "route53:ListHostedZonesByName",
                "route53:ListHostedZones"
            ],
            "Resource": "*"
        }
    ]
}
```
