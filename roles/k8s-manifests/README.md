# To add your own manifests

Simply create ``roles/k8s-manifests/templates/your-branch`` to match the name of your environment, same one you use in ``inventory/your-branch``.

Then reference them with the full path in your ``group_vars/k8s.yml``, for example;

```yaml
manifests:
  - file: your-branch/ingress.yaml
    state: present
```
