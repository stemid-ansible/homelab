server:
	interface: {{ unbound_interface }}
	#interface: 127.0.0.1@5353	# listen on alternative port
	#interface: ::1
	do-ip6: no

	# override the default "any" address to send queries; if multiple
	# addresses are available, they are used randomly to counter spoofing
	#outgoing-interface: 192.0.2.1
	#outgoing-interface: 2001:db8::53

	access-control: 0.0.0.0/0 refuse
	access-control: 127.0.0.0/8 allow
	access-control: ::0/0 refuse
	access-control: ::1 allow

	access-control: 192.168.122.0/24 allow
	access-control: 192.168.22.0/24 allow
	access-control: 192.168.21.0/24 allow

	# VPN subnet must be allowed
	access-control: 10.6.6.0/24 allow

	hide-identity: yes
	hide-version: yes

	# Perform DNSSEC validation.
	#
	auto-trust-anchor-file: "/var/unbound/db/root.key"
	val-log-level: 2

	# Synthesize NXDOMAINs from DNSSEC NSEC chains.
	# https://tools.ietf.org/html/rfc8198
	#
	aggressive-nsec: yes

	# Serve zones authoritatively from Unbound to resolver clients.
	# Not for external service.
	#
	#local-zone: "local." static
	#local-data: "mycomputer.local. IN A 192.0.2.51"
	#local-zone: "2.0.192.in-addr.arpa." static
	#local-data-ptr: "192.0.2.51 mycomputer.local"
	
{% for zone in local_zones %}
	local-zone: "{{ zone.name }}." redirect
{% for host in zone.hosts %}
	local-data: "{{ host.name }}. IN A {{ host.ip }}"
{% endfor %}
{% endfor %}

	local-zone: "{{ local_domain }}." static

{% for dev in local_devices %}
	local-data: "{{ dev.name }}.{{ local_domain }}. IN A {{ dev.ip }}"
{% endfor %}

	# Use TCP for "forward-zone" requests. Useful if you are making
	# DNS requests over an SSH port forwarding.
	#
	#tcp-upstream: yes

	# CA Certificates used for forward-tls-upstream (RFC7858) hostname
	# verification.  Since it's outside the chroot it is only loaded at
	# startup and thus cannot be changed via a reload.
	#tls-cert-bundle: "/etc/ssl/cert.pem"

forward-zone:
	name: "."
	forward-tls-upstream: yes
{% for server in upstream_dns_servers %}
	forward-addr: {{ server.address }}@{{ server.port }}
{% endfor %}

remote-control:
	control-enable: yes
	control-interface: /var/run/unbound.sock

# Use an upstream forwarder (recursive resolver) for some or all zones.
#
#forward-zone:
#	name: "."				# use for ALL queries
#	forward-addr: 192.0.2.53		# example address only
#	forward-first: yes			# try direct if forwarder fails

# Use an upstream DNS-over-TLS forwarder and do not fall back to cleartext
# if that fails.
#forward-zone:
#	name: "."
#	forward-tls-upstream: yes		# use DNS-over-TLS forwarder
#	forward-first: no			# do NOT send direct
#	# the hostname after "#" is not a comment, it is used for TLS checks:
#	forward-addr: 192.0.2.53@853#resolver.hostname.example
